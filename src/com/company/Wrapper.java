package com.company;

public class Wrapper {
    double value ;
    Wrapper(double value){
        this.value = value;
    }
    public String toString(){
        return  Double.toString(value);
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}

