package com.company;

public class Main {

    public static void main(String[] args) {
	    Wrapper wrapper = new Wrapper(4.00);
        System.out.println("Wrapper method");
        Object valueGiven = wrapper.getValue();
        System.out.println(wrapper.getValue() + " type of " + valueGiven.getClass().getSimpleName());
        System.out.println( wrapper + " from " + wrapper.getClass().getSimpleName() + " of type String");
    }
}
